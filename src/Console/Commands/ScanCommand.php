<?php
namespace Elfet\Components\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ScanCommand extends Command {
    /**
       * The console command name.
       *
       * @var string
       */
      protected $name = 'lc:scan';

      /**
       * The console command description.
       *
       * @var string
       */
      protected $description = 'Enable module.';

      /**
       * Name of directory that will contain the modules
       *
       * @var string
       */
      protected $directory;

      /**
       * Execute the console command.
       *
       * @return mixed
       */
      public function fire() {

          $modules_path = config('components.modules_path', false);

          if($modules_path) {
              $modules_path = base_path($modules_path);

              $modules = $this->getModules($modules_path);

              if(Cache::has('elfet_modules')) Cache::forget('elfet_modules');
              if($modules && count($modules) > 0) Cache::forever('elfet_modules', json_encode($modules));

              return $this->info('Scan command was successfully executed.');
          }
      }

      private function getModules($modules_path) {
          $modules_directories = $this->laravel->files->directories($modules_path);
          $modules = [];

          if(count($modules_directories) > 0) {
              foreach ($modules_directories as $module_directory) {
                  $module = $this->getModule($module_directory);

                  if($module) {
                      $modules[] = $module;
                  }
              }
          }

          return $modules;
      }


      private function getModule($module_directory) {
          $module = false;

          if($this->laravel->files->exists($module_directory . '/module.json')) {
              $module_json = $this->laravel->files->get($module_directory . '/module.json');
              $module_json = json_decode($module_json, true);

              if($module_json && json_last_error() == JSON_ERROR_NONE) {
                  $module_json['components'] = $this->getComponents($module_directory);
                  $module = $module_json;
              }
          }

          return $module;
      }


      private function getComponents($module_directory) {
          $components_directories = $this->laravel->files->directories($module_directory);
          $components = [];

          if(count($components_directories) > 0) {
              foreach ($components_directories as $component_directory) {
                  $component = $this->getComponent($component_directory);

                  if($component) {
                      $components[] = $component;
                  }
              }
          }

          return $components;
      }


      private function getComponent($component_directory) {
          $component = false;

          if($this->laravel->files->exists($component_directory . '/component.json')) {
              $component_json = $this->laravel->files->get($component_directory . '/component.json');
              $component_json = json_decode($component_json, true);

              if($component_json && json_last_error() == JSON_ERROR_NONE) {
                  $component = $component_json;
              }
          }

          return $component;
      }
}
