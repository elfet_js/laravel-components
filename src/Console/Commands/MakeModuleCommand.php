<?php
namespace Elfet\Components\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeModuleCommand extends Command {
    /**
       * The console command name.
       *
       * @var string
       */
      protected $name = 'lc:make-module';

      /**
       * The console command description.
       *
       * @var string
       */
      protected $description = 'Make new module.';

      /**
       * Execute the console command.
       *
       * @return mixed
       */
      public function fire() {
          $modules_path = config('components.modules_path', false);
          $modules_path = base_path($modules_path);
          $module = [];

          $module['name'] = ucfirst($this->ask('Enter the name of new module.'));
          $module['description'] = $this->ask('Enter the description of new module.');
          $module['priority'] = $this->ask('Enter the boot priority of new module.');
          $module['enabled'] = $this->confirm('Set module as active?');

          $validation = Validator::make($module, [
              'name' => 'required|string',
              'description' => 'string',
              'priority' => 'required|integer',
              'enabled' => 'required|boolean'
          ]);

          if($validation->fails()) {
              $messages = current($validation->messages()->toArray());
              return $this->error($messages[0]);
          }

          if($this->laravel->files->exists($modules_path . '/' .$module['name'])) {
              return $this->error('Module ' . $module['name'] . ' allready exists.');
          }

          if(!$this->laravel->files->makeDirectory($modules_path . '/' . $module['name'], 0777)) {
              return $this->error('Could not create directory for new module.');
          }

          $module_json = json_encode($module, JSON_PRETTY_PRINT);

          if(!$this->laravel->files->put($modules_path . '/' . $module['name'] . '/module.json', $module_json)) {
              return $this->error('Could not create module.json file for module.');
          }

          return $this->info('Module ' . $module['name'] . ' successfully created.');
      }
}
