<?php
namespace Elfet\Modules\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class EnableModuleCommand extends Command {
    /**
       * The console command name.
       *
       * @var string
       */
      protected $name = 'lc:enable-module';

      /**
       * The console command description.
       *
       * @var string
       */
      protected $description = 'Enable module.';

      /**
       * Name of directory that will contain the modules
       *
       * @var string
       */
      protected $directory;

      /**
       * Execute the console command.
       *
       * @return mixed
       */
      public function fire() {

      }
}
