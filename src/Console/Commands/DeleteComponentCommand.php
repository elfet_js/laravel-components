<?php
namespace Elfet\Modules\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class DeleteModuleCommand extends Command {
    /**
       * The console command name.
       *
       * @var string
       */
      protected $name = 'lc:delete-component';

      /**
       * The console command description.
       *
       * @var string
       */
      protected $description = 'Delete component.';

      /**
       * Name of directory that will contain the modules
       *
       * @var string
       */
      protected $directory;

      /**
       * Execute the console command.
       *
       * @return mixed
       */
      public function fire() {

      }
}
