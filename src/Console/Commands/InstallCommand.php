<?php
namespace Elfet\Components\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class InstallCommand extends Command {
    /**
       * The console command name.
       *
       * @var string
       */
      protected $name = 'lc:install';

      /**
       * The console command description.
       *
       * @var string
       */
      protected $description = 'Install the Laravel Components package.';

      /**
       * Name of directory that will contain the modules
       *
       * @var string
       */
      protected $directory;

      /**
       * Execute the console command.
       *
       * @return mixed
       */
      public function fire() {
          if(config('components.modules_path', false)) {
             return $this->error('Laravel Components package allready installed.');
          }

          $modules_path = $this->ask('Enter the name of modules directory.', 'Modules');

          $validation = Validator::make(['modules_path' => $modules_path], [
              'modules_path' => 'required|string|not_in:app,bootstrap,config,database,public,resources,storage,vendor'
          ]);

          if($validation->fails()) {
              $messages = current($validation->messages()->toArray());
              return $this->error($messages[0]);
          }

          if(!$this->laravel->files->exists(base_path($modules_path))) {
              $this->laravel->files->makeDirectory(base_path($modules_path), 0777);
          }

          $config = $this->laravel->files->get(__DIR__ . '/../../../config/components.php');
          $config = str_replace('{% DIRECTORY %}', $modules_path, $config);

          $this->laravel->files->put(config_path('components.php'), $config);

          return $this->info('Laravel Components Package was successfully installed.');
      }
}
