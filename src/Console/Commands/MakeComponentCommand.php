<?php
namespace Elfet\Components\Console\Commands;

use ZipArchive;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeComponentCommand extends Command {
    /**
       * The console command name.
       *
       * @var string
       */
      protected $name = 'lc:make-component {name} {module}';

      /**
       * The console command description.
       *
       * @var string
       */
      protected $description = 'Make new component in module.';

      protected function getArguments(){
            return [
                ['name', InputArgument::REQUIRED],
                ['module', InputArgument::REQUIRED]
            ];
        }

      /**
       * Execute the console command.
       *
       * @return mixed
       */
      public function fire() {
          $modules_path = config('components.modules_path', false);
          $modules_path = base_path($modules_path);

          $name = ucfirst($this->argument('name'));
          $module = ucfirst($this->argument('module'));

          $validation = Validator::make(['name' => $name, 'module' => $module], [
              'name' => 'required|string',
              'module' => 'required|string'
          ]);

          if($validation->fails()) {
              $messages = current($validation->messages()->toArray());
              return $this->error($messages[0]);
          }

          if(!$this->laravel->files->exists($modules_path . '/' . $module)) {
              return $this->error('Module ' . $module . ' does not exists.');
          }

          if($this->laravel->files->exists($modules_path . '/' . $module . '/' . $name)) {
              return $this->error('Component allready exists in ' . $module . ' module.');
          }

          $description = $this->ask('Enter the description of component.', '');
          $priority = $this->ask('Set the boot priority of component.', 0);
          $enabled = $this->confirm('Set component as active?');

          $validation = Validator::make([
              'priority' => $priority,
              'enabled' => $enabled,
              'description' => $description
          ], [
              'description' => 'string',
              'priority' => 'required|integer',
              'enabled' => 'required|boolean'
          ]);

          if($validation->fails()) {
              $messages = current($validation->messages()->toArray());
              return $this->error($messages[0]);
          }

          $component = [
              'name' => $name,
              'module' => $module,
              'description' => $description,
              'priority' => $priority,
              'enabled' => $enabled,
              'path' => $modules_path . '/' . $module . '/' . $name
          ];

          return $this->makeComponent($component);
      }


      private function makeComponent($component) {
          $modules_path = config('components.modules_path', false);

          if(!$this->laravel->files->makeDirectory($component['path'], 0777)) {
              return $this->error('Could not create directory for component.');
          }

          $zip = new ZipArchive;

          if(!$zip->open(__DIR__ . '/../Blanks/Component.zip')) {
              return $this->error('Could not read zip archive.');
          }

          $zip->extractTo($component['path']);
          $zip->close();

          $provider = $this->laravel->files->get($component['path'] . '/Providers/ComponentServiceProvider.php');
          $provider = str_replace('{% COMPONENT %}', $component['name'], $provider);
          $provider = str_replace('{% MODULE %}', $component['module'], $provider);
          $this->laravel->files->put($component['path'] . '/Providers/ComponentServiceProvider.php', $provider);

          $component_json = [
              'name' => $component['name'],
              'description' => $component['description'],
              'priority' => $component['priority'],
              'enabled' => $component['enabled'],
              'provider' => '\\' . implode('\\', [
                  ucfirst($modules_path),
                  ucfirst($component['module']),
                  ucfirst($component['name']),
                  'Providers',
                  'ComponentServiceProvider'
              ])
          ];

          if(!$this->laravel->files->put($component['path'] . '/component.json', json_encode($component_json, JSON_PRETTY_PRINT))) {
             return $this->error('Could not create component.json file.');
          }

          $this->callSilent('lc:scan');

          return $this->info('Component was successfully created.');
      }
}
