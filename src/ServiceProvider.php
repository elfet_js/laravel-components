<?php
namespace Elfet\Components;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use Elfet\Components\Providers\ComponentsServiceProvider;
use Elfet\Components\Providers\ConsoleServiceProvider;

class ServiceProvider extends LaravelServiceProvider {
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->publishes([ __DIR__ . '/../../config/components.php' => config_path('components.php') ], 'modules');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        App::register(ComponentsServiceProvider::class);

        if(App::runningInConsole()) {
            App::register(ConsoleServiceProvider::class);
            }
    }
}
