<?php
namespace Elfet\Components\Providers;

use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider {
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Parent command namespace.
     *
     * @var string
     */
    protected $namespace = 'Elfet\\Components\\Console\\Commands\\';

    /**
     * The available command shortname.
     *
     * @var array
     */
    protected $commands = [
        'Install',
        'Scan',
        'MakeModule',
        'MakeComponent'
    ];

	/**
    * Register the commands.
    */
    public function register() {
        foreach ($this->commands as $command) {
            $this->commands($this->namespace.$command.'Command');
        }
    }

    /**
     * @return array
     */
    public function provides() {
        $provides = [];

        foreach ($this->commands as $command) {
            $provides[] = $this->namespace . $command . 'Command';
        }

        return $provides;
    }
}
