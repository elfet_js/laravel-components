<?php
namespace Elfet\Components\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Elfet\Components\Collections\Modules;
use Elfet\Components\Entities\Module;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;


class ComponentsServiceProvider extends LaravelServiceProvider {
    public function register() {
        App::singleton('modules', function ($app) {
            return new Modules(null);
        });

        $this->loadModulesFromCache();
    }

    public function boot() {
        $this->app->modules->sortBy(function($item) {
            return $item->priority;
        })->each(function(&$module){
            $module->boot();
        });
    }

    protected function loadModulesFromCache() {
        if(Cache::has('elfet_modules')) {
            $modules = json_decode(Cache::get('elfet_modules'), true);

            if($modules && json_last_error() == JSON_ERROR_NONE) {
                foreach ($modules as $item) {
                    $this->app->modules->push((new Module($item)));
                }
            }
        }
    }
}
