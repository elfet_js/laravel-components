<?php
namespace Elfet\Components\Entities;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Elfet\Components\Repositories\Components;

class Component {
    public $name;
    public $description;
    public $priority;
    private $module;
    private $enabled;
    private $booted = false;

    public function __construct($component) {
        $validation = Validator::make($component, [
            'name' => 'required|string',
            'description' => 'string',
            'priority' => 'required|integer',
            'enabled' => 'required|boolean'
        ]);

        if($validation->fails()) {
            $message = current($validation->messages()->toArray());

            throw new \InvalidArgumentException($message[0]);
        }

        $this->name = $component['name'];
        $this->description = $component['description'];
        $this->priority = $component['priority'];
        $this->enabled = $component['enabled'];
    }

    public function boot($module) {
        if($this->enabled && !$this->booted) {
            $componentProvider = ucfirst(config('components.modules_path', 'Modules'));
            $componentProvider .= '\\' . ucfirst($module->name) . '\\' . ucfirst($this->name);
            $componentProvider .= '\\Providers\\ComponentServiceProvider';

            if(class_exists($componentProvider)) {
                App::register($componentProvider);
            }

            $this->booted = true;
		}
    }

    public function __get($property) {
        if(!property_exists($this, $property)) {
            throw new Exception('Undefined property "'.$property.'" should be get');
        } else {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if(!property_exists($this, $property)) {
            throw new \Exception('Undefined property "'.$property.'" should be set to "'.$value.'"');
        } else {
            $this->$property = $value;
            return $this;
        }
    }
}
