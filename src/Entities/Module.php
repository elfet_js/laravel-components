<?php
namespace Elfet\Components\Entities;

use Elfet\Components\Entities\Component;
use Illuminate\Support\Facades\Validator;
use Elfet\Components\Collections\Components;

class Module {
    public $name;
    public $description;
    public $priority;
    private $enabled;
    public $components;
    private $booted = false;

    public function __construct($module) {
        $validation = Validator::make($module, [
            'name' => 'required|string',
            'description' => 'string',
            'priority' => 'required|integer',
            'enabled' => 'required|boolean',
            'components' => 'array'
        ]);

        if($validation->fails()) {
            $message = current($validation->messages()->toArray());

            throw new \InvalidArgumentException($message[0]);
        }

        $this->name = $module['name'];
        $this->description = $module['description'];
        $this->priority = $module['priority'];
        $this->enabled = $module['enabled'];

        $this->components = new Components();

        if(count($module['components'])) {

            foreach ($module['components'] as $item) {
                $this->components->push((new Component($item)));
            }
        }
    }

    public function boot() {
        if($this->enabled && !$this->booted) {
            if($this->components->count() > 0) {
                $this->components->sortBy(function($item) {
                    return $item->priority;
                })->each(function(&$component) {
                    $component->boot($this);
                });
            }

            $this->booted = true;
		}
    }

    public function __get($property) {
        if(!property_exists($this, $property)) {
            throw new Exception('Undefined property "'.$property.'" should be get');
        } else {
            return $this->$property;
        }
    }

    public function __set($property, $value) {
        if(!property_exists($this, $property)) {
            throw new \Exception('Undefined property "'.$property.'" should be set to "'.$value.'"');
        } else {
            $this->$property = $value;
            return $this;
        }
    }
}
